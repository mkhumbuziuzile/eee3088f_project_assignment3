# EEE3088F_Project_Assignment3
Micro PiHAT Project

The Project should be  opened using a designing software KiKad.It contains files of a Motor Driver Micro PiHAT

To download the software use link: [https://www.kicad.org/download/]

The modules we separately desingned and combined to creat the pcb

Separate Module schematics

    LED STATUS.sch

    PSU.sch

    Amplifier.sch

The Micro PiHAT schematic is 

    EEE3088F project.sch

The  Micro PiHAT pcb:

    EEE3088 project.kicad_pcb

To view all the complete project it is advisible to open the EEE3088F project.pro which will then show all the different modules in the KiKad software

The added libraried used can be found in the added lib folder, these will need to be imported to show the complete version on the microHAT

    NE555:          added lib\NE555

    OP1177:         added lib\OP1177
    
    DRV8313PWP:     added lib\DRV8313PWP 



