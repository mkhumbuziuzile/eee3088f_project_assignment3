EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Signal Amplifier"
Date "2021/06/03"
Rev "1.0"
Comp "EEE3088F Project"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R28
U 1 1 60BFDAA2
P 5950 3700
F 0 "R28" V 5743 3700 50  0000 C CNN
F 1 "10k" V 5834 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5880 3700 50  0001 C CNN
F 3 "~" H 5950 3700 50  0001 C CNN
	1    5950 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R26
U 1 1 60BFE3FC
P 5350 3250
F 0 "R26" H 5420 3296 50  0000 L CNN
F 1 "10k" H 5420 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5280 3250 50  0001 C CNN
F 3 "~" H 5350 3250 50  0001 C CNN
	1    5350 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R27
U 1 1 60BFEC69
P 5350 3950
F 0 "R27" H 5420 3996 50  0000 L CNN
F 1 "10k" H 5420 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5280 3950 50  0001 C CNN
F 3 "~" H 5350 3950 50  0001 C CNN
	1    5350 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3700 6350 3700
Wire Wire Line
	6350 3700 6350 3150
Wire Wire Line
	6350 3150 6250 3150
Wire Wire Line
	5800 3700 5350 3700
Wire Wire Line
	5350 3700 5350 3400
Wire Wire Line
	5350 3800 5350 3700
Connection ~ 5350 3700
Wire Wire Line
	5650 3050 5350 3050
Wire Wire Line
	5350 3050 5350 3100
Wire Wire Line
	5650 3250 5550 3250
Wire Wire Line
	5550 3250 5550 2650
Wire Wire Line
	6350 3150 6500 3150
Connection ~ 6350 3150
Wire Wire Line
	5850 3450 5850 4350
Wire Wire Line
	5850 4350 5550 4350
Wire Wire Line
	5350 4350 5350 4100
Text HLabel 6500 3150 2    50   Input ~ 10
Output_signal
$Comp
L power:+3.3V #PWR029
U 1 1 60C306BF
P 5850 2850
F 0 "#PWR029" H 5850 2700 50  0001 C CNN
F 1 "+3.3V" H 5865 3023 50  0000 C CNN
F 2 "" H 5850 2850 50  0001 C CNN
F 3 "" H 5850 2850 50  0001 C CNN
	1    5850 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR027
U 1 1 60C30E88
P 5350 3050
F 0 "#PWR027" H 5350 2900 50  0001 C CNN
F 1 "+3.3V" H 5365 3223 50  0000 C CNN
F 2 "" H 5350 3050 50  0001 C CNN
F 3 "" H 5350 3050 50  0001 C CNN
	1    5350 3050
	1    0    0    -1  
$EndComp
Connection ~ 5350 3050
Text HLabel 5550 2650 0    50   Input ~ 10
Input_Hall_signal
NoConn ~ 5950 3450
NoConn ~ 6050 3450
$Comp
L power:GND #PWR028
U 1 1 60C31809
P 5550 4350
F 0 "#PWR028" H 5550 4100 50  0001 C CNN
F 1 "GND" H 5555 4177 50  0000 C CNN
F 2 "" H 5550 4350 50  0001 C CNN
F 3 "" H 5550 4350 50  0001 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Connection ~ 5550 4350
Wire Wire Line
	5550 4350 5350 4350
$Comp
L Amplifier_Operational:AD8001AN U2
U 1 1 60BB6256
P 5950 3150
F 0 "U2" H 6294 3196 50  0000 L CNN
F 1 "AD8001AN" H 6294 3105 50  0000 L CNN
F 2 "Package_DFN_QFN:DFN-8_2x2mm_P0.5mm" H 5850 2950 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ad8001.pdf" H 6100 3300 50  0001 C CNN
	1    5950 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
